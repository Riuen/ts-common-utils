import {deepFind} from '../src';

describe('Deep Find', () => {

  test('should be able to find the element in a nested object.', () => {
    const obj = {
      id: 1,
      user: {
        address: {
          route: {
            id: 12,
            name: 'route12'
          }
        }
      }
    };

    expect(deepFind<string>(obj, 'user.address.route.name')).toEqual('route12');
  });

  test('should return the supplied object if path is empty.', () => {
    const obj = {
      id: 1,
      user: {
        address: {
          route: {
            id: 12,
            name: 'route12'
          }
        }
      }
    };

    expect(deepFind(obj, '')).toEqual(obj);
  });

  test('should fail if the path does not exist.', () => {
    const obj = {
      id: 1,
      user: {
        address: {
          route: {
            id: 12,
            name: 'route12'
          }
        }
      }
    };

    expect(() => deepFind(obj, 'id.user.invalid')).toThrow(new Error('Path does not exist on specified object.'));
  });

  test('should return undefined if the path does not exist and "failOnError" option is false.', () => {
    const obj = {
      id: 1,
      user: {
        address: {
          route: {
            id: 12,
            name: 'route12'
          }
        }
      }
    };

    expect(deepFind(obj, 'id.user.invalid', {failOnError: false})).toBeUndefined();
  });

  test('should fail if an object is found to be an array.', () => {
    const obj = [{
      id: 1,
      user: {
        address: {
          route: {
            id: 12,
            name: 'route12'
          }
        }
      }
    }];

    expect(() => deepFind(obj, 'id.user.array')).toThrow(new Error('Unsupported object type "Array" passed.'));
  });

  test('should return null if the value at the path specified is null.', () => {
    const obj = {
      id: 1,
      user: {
        address: {
          route: {
            id: null,
            name: 'route12'
          }
        }
      }
    };

    expect(deepFind(obj, 'user.address.route.id')).toBeNull();
  });

  test('should return undefined if the value at the path specified is undefined.', () => {
    const obj = {
      id: 1,
      user: {
        address: {
          route: {
            id: undefined,
            name: 'route12'
          }
        }
      }
    };

    expect(deepFind(obj, 'user.address.route.id')).toBeUndefined();
  });
});
