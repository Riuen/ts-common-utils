import {patchObject} from '../src';

describe('Object Patch', () => {

  test('should update the target object with the value of matching keys from the source object.', () => {
    const target = {
      id: 1,
      name: 'obj1',
      status: null,
    };
    const source = {
      id: 2,
      name: 'obj3',
      status: 'test2',
      category: 'science'
    };
    patchObject(target, source);
    expect(target).toEqual({id: 2, name: 'obj3', status: 'test2'});
  });

  test('should partially update nest structures inorder to preserve the structure of the target.', () => {
    const target = {
      id: 1,
      name: 'obj1',
      status: 'test',
      user: {
        id: 1,
        name: 'user',
      }
    };
    const source = {
      id: 2,
      name: 'obj3',
      status: 'test2',
      category: 'science',
      user: {
        id: 2,
        name: 'user2',
        status: 'testuser'
      }
    };
    patchObject(target, source);
    expect(target).toEqual({id: 2, name: 'obj3', status: 'test2', user: {id: 2, name: 'user2'}});
  });
});
