import {extractObjectPaths} from '../src';

describe('Find object primitive key paths.', () => {
  test('should return all key paths for the object supplied', () => {
    const target = {
      id: 1,
      name: 'obj1',
      status: 'test',
      user: {
        id: 1,
        name: 'user',
        category: {
          id: 'c1',
          location: {
            city: 'city1'
          }
        }
      }
    };
    const paths = extractObjectPaths(target);
    const expectedPaths = [
      'id', 'name', 'status', 'user.id', 'user.name', 'user.category.id', 'user.category.location.city'
    ]
    expect(paths).toEqual(expectedPaths);
  });
});
