import {arrayPeek} from '../src/array-utils';

describe('Array Peek', () => {
  test('should return the first value of an array if it exists', () => {
    expect(arrayPeek([1,2])).toEqual(1);
  });

  test('should return null if the array is undefined', () => {
    expect(arrayPeek(undefined)).toBeNull();
  });

  test('should return the value at the first index even if the first index of the array array is undefined', () => {
    expect(arrayPeek([undefined, 1])).toEqual(undefined);
  });
});
