import {isEmpty} from '../src';

describe('isEmpty', () => {

  test('should be true when empty array', () => {
    expect(isEmpty([])).toEqual(true)
  });

  test('should be true when value is empty string', () => {
    expect(isEmpty('')).toEqual(true)
  });

  test('should be true when value is empty object', () => {
    expect(isEmpty({})).toEqual(true)
  });

  test('should be true when value is undefined', () => {
    expect(isEmpty(undefined)).toEqual(true)
  });

  test('should be true when value is null', () => {
    expect(isEmpty(null)).toEqual(true)
  });

  test('should be true when value is empty map', () => {
    expect(isEmpty(new Map())).toEqual(true)
  });

  test('should be false when value is instance of date', () => {
    expect(isEmpty(new Date())).toEqual(false)
  });

  test('should be false when value is object with at-least 1 key', () => {
    expect(isEmpty({o:1})).toEqual(false)
  });

  test('should be false when value is array with numeric string zero', () => {
    expect(isEmpty(['0'])).toEqual(false)
  });

  test('should be false when value is zero', () => {
    expect(isEmpty('0')).toEqual(false)
  });

  test('should be false when string contains at-least 1 character', () => {
    expect(isEmpty('k')).toEqual(false)
  });

  test('should be false when value is zero', () => {
    expect(isEmpty(0)).toEqual(false)
  });

  test('should be false when value is valid boolean true', () => {
    expect(isEmpty(true)).toEqual(false)
  });

  test('should be false when value is valid boolean false', () => {
    expect(isEmpty(false)).toEqual(false)
  });

  test('should be false when value is map with at-least 1 entry', () => {
    const map = new Map();
    map.set('a', 1);
    expect(isEmpty(map)).toEqual(false)
  });
});
