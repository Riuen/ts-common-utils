import {arrayElementAt, arrayLastElementOf} from '../src/array-utils';

describe('Array item retrieval', () => {
  test('should return the item at the specified index', () => {
    const source = [1,2,3];
    expect(arrayElementAt(source, 1)).toEqual(2);
  });

  test('should return null if the specified index does not exist', () => {
    const source = [1,2,3];
    expect(arrayElementAt(source, 5)).toBeNull();
  });

  test('should return the last element in the array', () => {
    const source = [1,2,3];
    expect(arrayLastElementOf(source)).toEqual(3);
  });

  test('should return null if the array is undefined', () => {
    expect(arrayElementAt(undefined, 2)).toBeNull();
  });

  test('should return null if the array is undefined', () => {
    expect(arrayLastElementOf(undefined)).toBeNull();
  });
});
