import {arrayDistinct} from '../src/array-utils';

describe('Distinct Array', () => {
  test('should be able to return a distinct array when no path is specified. AKA array of primatives.', () => {
    const source = [1, 2, 3, 6, 3, 3, 2, 9];
    expect(arrayDistinct(source)).toEqual([1, 2, 3, 6, 9]);
  });

  test('should be able to return a distinct array filtered by the id supplied', () => {
    const source = [
      {name: 'Brian'},
      {name: 'Joe'},
      {name: 'Joe'},
      {name: 'Sue'}
    ];
    expect(arrayDistinct(source, 'name')).toEqual([
      {name: 'Brian'},
      {name: 'Joe'},
      {name: 'Sue'}
    ]);
  });
});
