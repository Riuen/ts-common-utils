import {arrayFilter} from '../src/array-utils';

describe('Array Filter', () => {
  test('should correctly filter the source using the property names specified', () => {
    const source = [
      {
        id: 1,
        name: 'apple'
      },
      {
        id: 2,
        name: 'pineapples'
      },
      {
        id: 3,
        name: 'cherry'
      }
    ];
    const filteredSource = arrayFilter(source, 'apple', ['name']);
    expect(filteredSource).toEqual([{id: 1, name: 'apple'}, {id: 2, name: 'pineapples'}]);
  });

  test('should return the source if no search key is provided', () => {
    const source = [
      {
        id: 1,
        name: 'apple'
      },
      {
        id: 2,
        name: 'pineapples'
      },
      {
        id: 3,
        name: 'cherry'
      }
    ];
    const filteredSource = arrayFilter(source, null, ['name']);
    expect(filteredSource).toEqual(source);
  });

  test('should return empty array if source is falsey', () => {
    const filteredSource = arrayFilter(null, 'apple', ['name']);
    expect(filteredSource).toEqual([]);
  });

  test('should correctly filter the source using the property names specified. EXACT MATCH', () => {
    const source = [
      {
        id: 1,
        name: 'apple',
      },
      {
        id: 2,
        name: 'pineapples'
      },
      {
        id: 3,
        name: 'cherry'
      }
    ];
    const filteredSource = arrayFilter(source, 'apple', ['name'], 'exact');
    expect(filteredSource).toEqual([{id: 1, name: 'apple'}]);
  });

  test('should be able to filter nested objects', () => {
    const source = [
      {
        id: 1,
        name: 'apple',
        category: {
          id: 'fruit'
        }
      },
      {
        id: 2,
        name: 'pineapples',
        category: {
          id: 'fruit'
        }
      },
      {
        id: 3,
        name: 'cherry',
        category: {
          id: 'fruit'
        }
      },
      {
        id: 4,
        name: 'cabbage',
        category: {
          id: 'vegetable'
        }
      }
    ];
    const filteredSource = arrayFilter(source, 'vegetable', ['category.id'], 'exact');
    expect(filteredSource).toEqual([{id: 4, name: 'cabbage', category: {id: 'vegetable'}}]);
  });

  test('should correctly filter the source using the property names specified even if the property is not a string. EXACT MATCH', () => {
    const source = [
      {
        id: 1,
        name: 'apple',
      },
      {
        id: 2,
        name: 'pineapples'
      },
      {
        id: 3,
        name: 'cherry'
      }
    ];
    const filteredSource = arrayFilter(source, '1', ['id'], 'exact');
    expect(filteredSource).toEqual([{id: 1, name: 'apple'}]);
  });

  test('should perform exact match if property is found to be a number', () => {
    const source = [
      {
        id: 1,
        name: 'apple',
      },
      {
        id: 2,
        name: 'pineapples'
      },
      {
        id: 3,
        name: 'cherry'
      }
    ];
    const filteredSource = arrayFilter(source, '1', ['id'], 'fuzzy');
    expect(filteredSource).toEqual([{id: 1, name: 'apple'}]);
  });
});
