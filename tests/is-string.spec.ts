import {isString} from '../src';

describe('isString', () => {

  test('should correctly detect when the input is a string', () => {
    expect(isString('helloworld')).toEqual(true);
  });

  it('should correctly detect that an array is not a string', () => {
    expect(isString([])).toEqual(false);
  });

  it('should correctly detect that an object is not a string', () => {
    expect(isString({id: 'hi'})).toEqual(false);
  });

  it('should correctly detect that a number is not a string', () => {
    expect(isString(42)).toEqual(false);
  });

  it('should correctly detect that a date is not a string', () => {
    expect(isString(new Date())).toEqual(false);
  });

  it('should correctly detect that a boolean is not a string', () => {
    expect(isString(true)).toEqual(false);
  });
});
