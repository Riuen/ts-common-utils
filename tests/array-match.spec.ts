import {arrayMatch} from '../src/array-utils';

describe('Array Match', () => {

  test('should be able to match nested objects when no unique key is set', () => {
    const listA = [
      { name: 'Brian', address: {street: 'street1', lot: 'lotA'} },
      { name: 'Joe' },
      { name: 'Joe' },
      { name: 'Sue' }
    ];
    const listB = [
      { name: 'Brian', address: {street: 'street1', lot: 'lotA'} },
      { name: 'Joe' },
      { name: 'Joe' },
      { name: 'Sue' }
    ];
    expect(arrayMatch(listA, listB)).toEqual(true);
  });

  test('should be able to match nested objects regardless of the order of the keys when no unique key is set', () => {
    const listA = [
      { name: 'Sue' },
      { name: 'Brian', address: {lot: 'lotA', street: 'street1'} },
      { name: 'Joe' },
      { name: 'Joe' }
    ];
    const listB = [
      { name: 'Brian', address: {street: 'street1', lot: 'lotA'} },
      { name: 'Joe' },
      { name: 'Joe' },
      { name: 'Sue' }
    ];
    expect(arrayMatch(listA, listB)).toEqual(true);
  });

  test('should not match if the number of elements are different in each array', () => {
    const listA = [
      { name: 'Sue' },
      { name: 'Brian', address: {lot: 'lotA', street: 'street1'} },
      { name: 'Joe' }
    ];
    const listB = [
      { name: 'Brian', address: {street: 'street1', lot: 'lotA'} },
      { name: 'Joe' },
      { name: 'Joe' },
      { name: 'Sue' }
    ];
    expect(arrayMatch(listA, listB)).toEqual(false);
  });

  test('should not match if the number of nested object elements are different in each array when no unique key is set', () => {
    const listA = [
      { name: 'Sue' },
      { name: 'Brian', address: {lot: 'lotA', street: 'street1'} },
      { name: 'Joe' }
    ];
    const listB = [
      { name: 'Brian', address: {street: 'street1'} },
      { name: 'Joe' },
      { name: 'Joe' },
      { name: 'Sue' }
    ];
    expect(arrayMatch(listA, listB)).toEqual(false);
  });

  test('should not match if the number of nested object elements are different in each array and no unique key is set', () => {
    const listA = [
      { name: 'Sue' },
      { name: 'Brian', address: {lot: 'lotA', street: 'street1'} },
      { name: 'Joe' }
    ];
    const listB = [
      { name: 'Brian', address: {street: 'street1'} },
      { name: 'Joe' },
      { name: 'Joe' },
      { name: 'Sue' }
    ];
    expect(arrayMatch(listA, listB)).toEqual(false);
  });

  test('should match the same unique key is present in both lists', () => {
    const listA = [
      { name: 'Sue', id: 1 },
      { name: 'Brian', address: {lot: 'lotA', street: 'street1'}, id: 2 },
      { name: 'Joe', id: 3 },
      { name: 'Alice', id: 4 }
    ];
    const listB = [
      { name: 'Brian', address: {street: 'street1'}, id: 2 },
      { name: 'Joe', id: 1 },
      { name: 'Joe', id: 3 },
      { name: 'Sue', id: 4 }
    ];

    expect(arrayMatch(listA, listB, 'id')).toEqual(true);
  });

  test('should not match if the same unique key is not present in both lists', () => {
    const listA = [
      { name: 'Sue', id: 1 },
      { name: 'Brian', address: {lot: 'lotA', street: 'street1'}, id: 2 },
      { name: 'Joe', id: 3 },
      { name: 'Alice', id: 5 }
    ];
    const listB = [
      { name: 'Brian', address: {street: 'street1'}, id: 2 },
      { name: 'Joe', id: 1 },
      { name: 'Joe', id: 3 },
      { name: 'Sue', id: 4 }
    ];

    expect(arrayMatch(listA, listB, 'id')).toEqual(false);
  });

  test('should not match if the same unique key is not present in both lists', () => {
    const listA = [
      { name: 'Sue', id: 1 },
      { name: 'Brian', address: {lot: 'lotA', street: 'street1'}, id: 2 },
      { name: 'Joe', id: 3 },
      { name: 'Alice', id: 5 }
    ];
    const listB = [
      [],
      { name: 'Joe', id: 1 },
      { name: 'Joe', id: 3 },
      { name: 'Sue', id: 4 }
    ];

    expect(arrayMatch(listA, listB, 'id')).toEqual(false);
  });

  test('should not match if the same unique key is not present in both lists', () => {
    const listA = [
      { name: 'Sue', id: undefined },
      { name: 'Brian', address: {lot: 'lotA', street: 'street1'}, id: 2 },
      { name: 'Joe', id: 3 },
      { name: 'Alice', id: 5 }
    ];
    const listB = [
      [],
      undefined,
      { name: 'Joe', id: 3 },
      { name: 'Sue', id: 4 }
    ];

    expect(arrayMatch(listA, listB, 'id')).toEqual(false);
  });
});
