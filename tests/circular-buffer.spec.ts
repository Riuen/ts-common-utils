import {CircularBuffer} from '../src/circular-buffer';

describe('Circular Buffer', () => {

  test('should store no more than the default max of 10.', () => {
    const cache = new CircularBuffer<number, string>();
    const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVXYZ';
    for (let i = 0; i <= 15; i++) {
      cache.add(i, alphabet.charAt(i));
    }
    expect(cache.size()).toEqual(10);
  });

  test('should always remove the oldest entries once the max size is reached', () => {
    const cache = new CircularBuffer<number, string>();
    const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVXYZ';
    let oldestRemoved = true;
    for (let i = 0; i < 10; i++) {
      cache.add(i, alphabet.charAt(i));
    }

    for (let i = 10; i < 20; i++) {
      let res = cache.has(i - 10);
      cache.add(i, alphabet.charAt(i));
      res = res && (cache.has(i - 10) === false);

      oldestRemoved = oldestRemoved || res;
    }

    expect(oldestRemoved).toEqual(true);
  });

  test('reset should successfully clear the buffer', () => {
    const cache = new CircularBuffer<number, string>();
    const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVXYZ';
    for (let i = 0; i <= 15; i++) {
      cache.add(i, alphabet.charAt(i));
    }
    cache.reset();
    expect(cache.size()).toEqual(0);
  });

  test('should store no more than the specified max.', () => {
    const cache = new CircularBuffer<number, string>(20);
    const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVXYZ1234567890';
    for (let i = 0; i <= 30; i++) {
      cache.add(i, alphabet.charAt(i));
    }
    expect(cache.size()).toEqual(20);
  });
});
