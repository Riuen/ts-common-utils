
[![pipeline status](https://gitlab.com/Riuen/ts-common-utils/badges/main/pipeline.svg)](https://gitlab.com/Riuen/ts-common-utils/-/commits/main)
[![coverage report](https://gitlab.com/Riuen/ts-common-utils/badges/main/coverage.svg)](https://gitlab.com/Riuen/ts-common-utils/-/commits/main)
# Typescript Common Utils
A collection of useful functions and tools for typescript/javascript.

## Testing
To execute the unit tests run `npm run test`

## Building
To build the library run `tsc`

## Packaging
After building the project run `npm pack` to package the compiled javascript into an installable package.
