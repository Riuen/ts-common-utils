/**
 * Checks if the specified value is empty. A value is considered empty if meets any of the following criteria:
 * - Is an empty string
 * - Length is equal to 0 (if array)
 * - Is ```undefined``` || ```null```
 * - Is an empty object ```{}```
 * @param value The value to check
 * @returns true/false
 */
export function isEmpty(value: any): boolean {

  if (value == null) {
    return true;
  }

  if (value instanceof Array) {
    return (value?.length === 0);
  }

  if (typeof value === 'string') {
    return (value?.trim().length === 0);
  }

  if (value instanceof Map) {
    return value.size === 0;
  }

  if (typeof value === 'object') {
    if (value instanceof Date) {
      return false;
    }
    return Object.keys(value).length === 0;
  }

  return false;
}


/**
 * Updates object1 with values from object2.
 *
 * Updates the values of the first object supplied, with the
 * values of matching keys from the second object supplied. This method
 * will attempt to preserve nested object structures found in object1.
 *
 * Note: if the target contains an array, it will be replaced with the array
 * found in the source.
 *
 * Example:
 *
 * ```
 * const o1 = {
 *  id: 1,
 *  name: "alice",
 *  role: {
 *    name: 'user'
 *  }
 * }
 *
 * const o2 = {
 *  id: 2,
 *  name: "bob",
 *  status: "A",
 *  role: {
 *    name: 'admin',
 *    timestamp: 'today'
 *  }
 * }
 *
 * patchObject(o1, o2);
 * Below shows what o1 would now reflect after objectPatch
 *  o1 = {
 *    id: 2,
 *    name: "bob",
 *    role: {
 *      name: 'admin'
 *    }
 *  }
 *
 * ```
 *
 * @param target
 * @param source
 */
export function patchObject<T>(target: T, source: any): void {
  if (isEmpty(target)) {
    return;
  }
  const sourceCopy = isEmpty(source) ? {} : source;

  for (const key in target) {
    if (!isEmpty(target[key]) && !Array.isArray(target[key]) && typeof target[key] === 'object') {
      const paths = extractObjectPaths(target[key], `${key}.`);
      for (const path of paths) {
        const updatedValue = deepFind(sourceCopy, path, {failOnError: false});
        if (updatedValue !== undefined) {
          updateElementAt(target, updatedValue, path);
        }
      }
    } else {
      target[key] = sourceCopy.hasOwnProperty(key) ? sourceCopy[key] : target[key];
    }
  }
}



/**
 * Searches an object and returns the element at the path specified.
 * @param obj The object to search.
 * @param path A dot delimited string representing the path to the desired element.
 * @param options
 * @returns
 */
export function deepFind<T>(obj: any, path: string, options: {failOnError: boolean} = {failOnError: true}): T {
  if (!path) {
    return obj;
  }

  const pathArr = path.split('.');
  if (pathArr.length === 1) {
    return obj[path];
  }

  if (obj instanceof Array && options.failOnError) {
    throw new Error('Unsupported object type "Array" passed.');
  }

  for (const key of pathArr) {
    if (!obj) {
      if (options.failOnError) {
        throw new Error('Path does not exist on specified object.');
      } else {
        return undefined;
      }
    }
    obj = obj[key];
  }
  return obj;
}


/**
 * Returns the paths of all primitive types for the object passed.
 * @param obj
 * @param path
 * @returns
 */
export function extractObjectPaths(obj: any, path = ''): string[] {
  let paths = [];
  for (const key in obj) {
    if ((obj[key] != null) && typeof obj[key] == 'object') {
      paths.push(...extractObjectPaths(obj[key], `${path}${key}.`));
    } else {
      paths.push(`${path}${key}`);
    }
  }
  return paths;
}

/**
 * Searches an object and returns the element at the path specified.
 * @param target The object to search.
 * @param value
 * @param path A dot delimited string representing the path to the desired element.
 * @returns
 */
function updateElementAt(target: any, value: any, path: string): void {
  if (!path) {
    return target;
  }

  let ref = target;
  const pathKeys = path.split('.');
  const terminationIdx = pathKeys.length - 1;
  for (let i = 0; i < terminationIdx; i++) {
    ref = ref[pathKeys[i]];
  }

  const targetKey = pathKeys[(pathKeys.length - 1)];
  if (ref.hasOwnProperty(targetKey)) {
    ref[targetKey] = value;
  }
}

/**
 * Checks if the supplied variable is a string
 * @param input
 */
export function isString (input: any): boolean {
  return (Object.prototype.toString.call(input) === '[object String]');
}
