import {isEmpty} from './object-utils';
import {arrayPeek} from './array-utils';

/**
 * This data structure uses an underlying map, designed to stored up to MAX_ENTRIES number of entries.
 * When the MAX_CACHE_SIZE is reached, subsequent writes will overwrite the oldest data.
 *
 * The primary use for this structure is to act as a fixed size cache to strike a balance between
 * speed of access and predictable memory requirements.
 */
export class CircularBuffer<K, V> {
  private _map: Map<K, V>;
  private readonly MAX_ENTRIES: number = 10;

  constructor(maxEntries?: number) {
    this._map = new Map();
    if (!isEmpty(maxEntries)) {
      this.MAX_ENTRIES = maxEntries;
    }
  }

  has(key: K): boolean {
    return this._map.has(key);
  }

  get(key: K): V {
    return this._map.get(key);
  }

  add(key: K, value: V): void {
    if (this._map.size >= this.MAX_ENTRIES) {
      const k1 = arrayPeek(Array.from(this._map.keys()));
      this._map.delete(k1);
    }
    this._map.set(key, value);
  }

  remove(key: K): void {
    this._map.delete(key);
  }

  reset(): void {
    this._map.clear();
  }

  size(): number {
    return this._map.size;
  }

  entries(): IterableIterator<[K, V]> {
    return this._map.entries();
  }

  keys(): IterableIterator<K> {
    return this._map.keys();
  }

  values(): IterableIterator<V> {
    return this._map.values();
  }

  forEach(cb: (value: V, key: K, map: Map<K, V>) => void, thisArg?: any): void {
    return this._map.forEach(cb, thisArg);
  }
}
