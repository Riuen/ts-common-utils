import {deepFind, isEmpty, isString} from './object-utils';

/**
 * Filters an array by matching the search key against the properties specified.
 *
 * @param source Array to search
 * @param searchKey Value to search for
 * @param searchPaths An array of dot delimited strings containing the paths of the search key elements to match against.
 * @param matchMode Fuzzy case in-sensitive search or exact match
 * @returns The filtered source.
 */
export function arrayFilter<T>(source: T[], searchKey: string, searchPaths: string[], matchMode: 'fuzzy' | 'exact' = 'fuzzy'): T[] {
  if (isEmpty(searchKey)) {
    return source;
  }

  searchKey = searchKey.toLowerCase();
  const searchFn = (matchMode === 'fuzzy')
    ? fuzzySearchFn
    : exactMatchFn

  return source?.filter(row => searchPaths.some(path => searchFn(row, path, searchKey))) || [];
}

/**
 * Returns the first element of the array. Returns `null` if the array is undefined or null.
 *
 * @param array
 */
export function arrayPeek<T>(array: Array<T>): T {
  return isEmpty(array) ? null : array[0];
}

/**
 * Returns the element at the index specified. If the index does not exist then `null`
 * will be returned.
 *
 * @param array
 * @param index
 */
export function arrayElementAt<T>(array: Array<T>, index: number): T {
  return (index < array?.length) ? array[index] : null;
}

/**
 * Returns the last element of the array. Returns `null` if the array is undefined or null.
 *
 * @param array
 * @returns
 */
export function arrayLastElementOf<T>(array: Array<T>): T {
  return isEmpty(array) ? null : array[(array.length - 1)];
}

/**
 * Returns all elements in the primary array that are not a part of the secondary array.
 * Returns any empty array if no differences are found.
 *
 * @param primaryArray
 * @param secondaryArray
 * @param primaryArrayKeyPath The path to the key that should be used to match against the secondaryArrayKey
 * @param secondaryArrayKeyPath The path to the key that should be used to match against the primaryArrayKey
 *
 * Note: For nested keys, use a dot limited string, eg: `user.id`
 */
export function arrayDiff<T, E>(primaryArray: Array<T>, secondaryArray: Array<E>, primaryArrayKeyPath: string, secondaryArrayKeyPath: string): Array<T> {
  return primaryArray.filter(pObj => secondaryArray.findIndex(sObj => deepFind(sObj, secondaryArrayKeyPath) === deepFind(pObj, primaryArrayKeyPath)) < 0) || [];
}

/**
 * Verifies if the 2 lists are equivalent irrespective of order. If a path to a unique key is specified,
 * then the match will be done against the unique key instead of all elements.
 *
 * @param listA
 * @param listB
 * @param uniqueKeyPath Key for the unique id that may be used for the comparison. For nested paths use dot
 * notation.
 * Eg `{user: {id: number, name: string}}`. The path to user id would be `user.id`
 * @returns true/false
 */
export function arrayMatch(listA: Array<any>, listB: Array<any>, uniqueKeyPath?: string): boolean {
  if (listA.length !== listB.length) {
    return false;
  }

  if (uniqueKeyPath) {
    try {
      const listBMap = new Map<any, any>();
      listB.forEach(b => listBMap.set(deepFind(b, uniqueKeyPath), b));
      return listA.every(a => listBMap.has(deepFind(a, uniqueKeyPath)));
    } catch (e) {
      return false;
    }
  }

  const listAString = stringify(listA);
  const listBString = stringify(listB);
  return listAString === listBString;
}

/**
 * Returns a new array with distinct elements based on the id set.
 *
 * @param array
 * @param idPath
 */
export function arrayDistinct<T>(array: Array<T>, idPath?: string): Array<T> {

  if (isEmpty(array)) {
    return array;
  }

  if (isEmpty(idPath)) {
    return [... new Set(array)];
  }

  let id: any;
  const uniqueIds = new Set();
  return array.filter(element => {
    id = deepFind(element, idPath);
    if (uniqueIds.has(id)) {
      return false;
    }
    uniqueIds.add(id);
    return true;
  });
}

function stringify<T>(value: T[]): string {
  return Array.from((JSON.stringify(value || []))).sort((a, b) => a.localeCompare(b)).join('');
}

function fuzzySearchFn<T>(record: T, path: string, searchKey: string): boolean {
  const value = deepFind<any>(record, path);
  if (isString(value)) {
    return value.toLowerCase().includes(searchKey);
  }
  return value == searchKey;
}

function exactMatchFn<T>(record: T, path: string, searchKey: string): boolean {
  return deepFind<any>(record, path) == searchKey;
}
